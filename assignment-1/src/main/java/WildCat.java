public class WildCat {

    // TODO Complete me!
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        // TODO Complete me!
        double bmi = weight / Math.pow(length / 100, 2);
        return bmi;
    }
}
