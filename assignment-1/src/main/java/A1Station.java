import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        // TODO Complete me!
        Scanner reader = new Scanner(System.in);
        int cats = reader.nextInt();
        reader.nextLine();
        String masukan;
        TrainCar head = null;
        for (int i = 0; i < cats; i++) {
            masukan = reader.nextLine();
            String[] splitted = masukan.split(",");
            String name = splitted[0];
            double weight = Double.parseDouble(splitted[1]);
            int length = Integer.parseInt(splitted[2]);
            WildCat cat = new WildCat(name, weight, length);
            if (head == null) {
                head = new TrainCar(cat);
            } else {
                TrainCar temp = new TrainCar(cat);
                if (head.computeTotalWeight() + temp.computeTotalWeight() > THRESHOLD) {
                    departTrain(head);
                    head = temp;
                } else {
                    temp.next = head;
                    head = temp;
                }
            }
        }
        if (head != null) {
            departTrain(head);
        }
    }

    public static void departTrain(TrainCar head) {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        head.printCar();
        System.out.println();
        double totalMassIndex = head.computeTotalMassIndex();
        double averageMassIndex = totalMassIndex / head.getLength();
        System.out.println("Average mass index of all cats: " + totalMassIndex);
        System.out.print("In average, the cats in the train are ");
        if (averageMassIndex < 18.5) {
            System.out.println("underweight");
        } else if (averageMassIndex >= 18.5 && averageMassIndex < 25) {
            System.out.println("normal");
        } else if (averageMassIndex >= 30) {
            System.out.println("overweight");
        }
    }
}
